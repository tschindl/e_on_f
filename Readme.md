SWT on FX
=========

What is it
==========

This is an experimental port of SWT built on JavaFX with the long distant target to get the Eclipse SDK running on JavaFX to exploit new UI features


How to contribute
=================

Please read https://wiki.eclipse.org/Efxclipse/SWTonFX